# Credit Card Fraud Detection

Dataset obtenido de Kaggle ([link](https://www.kaggle.com/mlg-ulb/creditcardfraud/)).

El dataset de este proyecto contiene transacciones realizadas con tarjetas de crédito en dos días de septiembre de 2013. Las variables de input son todas numéricas, y constan de: 
- El resultado de una transformación de PCA por motivos de confidencialidad (variables V1 - V28)
- Variable de *tiempo*: segundos transcurridos entre una transacción y la transacción inicial del dataset.
- Variable de *cantidad*: la cantidad de plata transferida.

En el proyecto comparo los resultados de correr una regresión logística y un Random Forest, concluyendo que el Random Forest tiene una leve mejoría en la performance teniendo menos errores tipo II, lo cual es importante en el contexto de detección de fraudes.

---

### WIP

- El dataset está altamente desbalanceado, ya que de las 284807 transacciones registradas, sólo 492 son fraudes (un %0.172 del total). Por lo tanto, un enfoque a considerar es aplicar alguna técnica de *resampling* para que el modelo entrene mejor.
- Detección de *outliers*: a tener en cuenta para el análisis exploratorio. De haber outliers aislados, su eliminación podría llevar a una mejor performance del modelo. Si existiera un cluster de observaciones, también mejoraría la performance del modelo identificarlos por medio de KMeans o algún modelo no supervisado de clustering y agregarlos en el proceso de feature engineering.
- El test ANOVA de *feature importance* muestra que muchas variables tienen muy poca relación con el target. Esto podría motivar la eliminación de esas variables para reducir la dimensionalidad del dataset de entrenamiento (en [este notebook](https://www.kaggle.com/gauravduttakiit/creditcard-fraud-detection-by-logistic-regression) se aplica backward elimination para este propósito).
