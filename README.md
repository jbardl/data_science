# Proyectos de ciencia de datos

Algunos de mis proyectos personales de ciencia de datos. En este repositorio se encuentran los siguientes archivos:

### Datasets de sklearn
- [Boston](https://gitlab.com/jbardl/data_science/-/blob/master/boston_dataset.ipynb): datos de precios inmobiliarios en Boston para regresión.
- [Iris](https://gitlab.com/jbardl/data_science/-/blob/master/iris_dataset.ipynb): datos sobre plantas del género *iris* para clasificación.
- [Wine](https://gitlab.com/jbardl/data_science/-/blob/master/wine_dataset.ipynb): datos con características de vinos para clasificación.
