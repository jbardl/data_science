# Ames Housing Dataset

En este repo se encuentran los datos y el notebook con mi presentación para la competencia de [predicción de precios inmobiliarios](https://www.kaggle.com/c/home-data-for-ml-course) de Kaggle. 

En el código realizo una limpieza del dataset, creo features para hacer explícitas algunas relaciones en los datos, extraigo features con modelos no supervisados, y finalmente ajusto un regresor XGBoost.

### WIP
Muchas de las features generadas a partir de los modelos no supervisados no se agregaron al dataset para evitar un gran aumento de la dimensionalidad del dataset final. Sin embargo, todavía quedan algunas alternativas por explorar, como por ejemplo aplicar un modelo de clustering al dataset con features obtenidas con PCA. Además, todavía falta realizar un tuneo de los hiperparámetros del modelo, que ahora sólo fueron elegidos *a priori*.
